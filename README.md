# Example CI Gitlab pipeline with integrated FossID WebApp scans

## How it works

The scripts in this repository contain a Gitlab job definition and a FossID WebApp API wrapper
that is set to scan this repository and its selected branch after every push to the origin
on Gitlab.

When a developer makes a new commit and pushes it to Gitlab, Gitlab will start a new job
that will start a new scan in the predefined FossID WebApp. The FossID scan will pull the code
from the branch to which the developer committed their changes. If the FossID scan detects
any new open source components (i.e. when the number of pending identifications is greater
than zero), the job will fail.

After the pending identifications are resolved in FossID WebApp, the job can be re-run
on Gitlab to make it pass.

The scripts create a new project in FossID WebApp and assign all scans to it. A new scan
is created for each branch and identifications are reused within the project. This way,
when a new identifications are made on a branch, the identifications are reused after
the branch is merged to master (main).

## Getting started

### Prerequisites

1. FossID WebApp needs to be running and the variables mentioned in the section above need to be
  defined in the Gitlab project.
2. FossID WebApp needs to be able to pull from the Gitlab git using ssh,
  i.e. proper certificates need to be configured on FossID WebApp server and on Gitlab.
3. FossID WebApp server needs to be reachable from Gitlab over http(s)

### Files in this project

This project contains three files that together form the CI/CD pipeline:

- `webapp_scan.py`: contains the scripts that automate FossID WebApp
    to run the scan in this project. The script uses FossID WebApp REST API.
- `requirements.txt`: dependencies for the script above
- `.gitlab-ci.yml`: Gitlab job definition

### Required environment variables

The scripts need the following environment variables to be set:

- `FOSSID_HOST`: the protocol and host of the FossID WebApp instance to be used for the scan.
- `FOSSID_USER`: user name of a user that can create and run scans in FossID WebApp.
- `FOSSID_TOKEN`: API token for the user above.

### Command line arguments

Usage:

```shell
webapp_scan.py [-h] [--ssh] [--unsafe-wait] git_url branch
```

Positional arguments:

- `git_url`: URL of the git repository
- `branch`: A branch to scan

`Optional arguments`:

- `--ssh`: force git connection over ssh even if the git url is with http(s).
- `--unsafe-wait`: Assume the scan finished when the progress reaches 100 percent. Work-around for bug GUI-2466.
- `-h`: print help

### Setting up a project with FossID WebApp scan on Gitlab

Create a new project or use an existing one. Copy the `webapp_scan.py`, and `requirements.txt`.

In your `gitlab-ci.yml`, add a new job that uses the `webapp_scan.py`:

```yml
fossid_scan:
  image: python:3.8
  before_script:
    - pip3 install -r requirements.txt
  script:
    - python3 webapp_scan.py --ssh $CI_REPOSITORY_URL $CI_COMMIT_REF_NAME
```

An example job is in the `.gitlab-ci.yml` in this project.

Note the arguments to `webapp_scan.py`:

- `$CI_REPOSITORY_URL` is provided by Gitlab. It is a public URL of the git repository with http protocol and authentication user name and token. Note that the user token in CI_REPOSITORY_URL changes with every job run and it cannot be updated in the FossID WebApp. That is why you either need to provide the `--ssh` command line argument to force the SSH connection, or provide a more stable git URL for the repository that contains the credentials. Still, you should be aware of the fact the credentials will be stored in plain text in FossID WebApp database and may appear in logs. Using git over HTTP is therefore strongly discouraged.
- `$CI_COMMIT_REF_NAME` - Gitlab provides the current branch name in this variable.
